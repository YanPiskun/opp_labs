﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Lib_Lab2;

namespace Lab2_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Сonversion();
        }

        private void Сonversion()
        {
            List<InspectorsLog> InspectorsLogs = InspectorsLog.LoadLogs();
            listBox1.Items.Clear();

            foreach (InspectorsLog log in InspectorsLogs)
            {
                listBox1.Items.Add(log);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                propertyGrid1.SelectedObject = listBox1.SelectedItem;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InspectorsLog.AddLogs(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, textBox6.Text);
            Сonversion();

            textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = textBox6.Text = "";
        }
    }
}
