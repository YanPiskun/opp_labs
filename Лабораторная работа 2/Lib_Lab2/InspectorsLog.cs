﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace Lib_Lab2
{
    public class InspectorsLog
    {
        public string LastName { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public DateTime DateOfVisit { get; set; }
        public string CompanyName { get; set; }
        public int AmountOfSanctions { get; set; }

        public InspectorsLog(string lastName, string name, string middleName,
            DateTime dateOfVisit, string companyName, int amountOfSanctions)
        {
            LastName = lastName;
            Name = name;
            MiddleName = middleName;
            DateOfVisit = dateOfVisit;
            CompanyName = companyName;
            AmountOfSanctions = amountOfSanctions;
        }

        public override string ToString()
        {
            return LastName + " " + Name + " " + MiddleName;
        }

        public static List<InspectorsLog> LoadLogs()
        {
            //Создаем список журналов.
            List<InspectorsLog> newLogs = new List<InspectorsLog>();

            using (XmlReader XR = XmlReader.Create(@"C:\Users\User\source\repos\OOP_Labs\Лабораторная работа 2\Lib_Lab2\XML.xml"))
            {
                string LastName = "";
                string Name = "";
                string MiddleName = "";
                DateTime DateOfVisit = DateTime.Now;
                string CompanyName = "";
                int AmountOfSanctions = 0;
                string element = "";

                while (XR.Read())
                {
                    // Считываем элемент.
                    if (XR.NodeType == XmlNodeType.Element)
                    {
                        //Получаем имя текущего элемента.
                        element = XR.Name;
                        if (element == "InspectorsLog")
                        {
                            LastName = XR.GetAttribute("LastName");
                        }
                    }
                    //Считывает значение элемента
                    else if (XR.NodeType == XmlNodeType.Text)
                    {
                        switch (element)
                        {
                            case "Name":
                                Name = XR.Value;
                                break;
                            case "MiddleName":
                                MiddleName = XR.Value;
                                break;
                            case "DateOfVisit":
                                DateOfVisit = DateTime.Parse(XR.Value);
                                break;
                            case "CompanyName":
                                CompanyName = XR.Value;
                                break;
                            case "AmountOfSanctions":
                                AmountOfSanctions = int.Parse(XR.Value);
                                break;
                        }
                    }
                    //Добавляет новые элемент в список.
                    else if ((XR.NodeType == XmlNodeType.EndElement) && (XR.Name == "InspectorsLog"))
                    {
                        newLogs.Add(new InspectorsLog(LastName, Name, MiddleName, DateOfVisit,
                            CompanyName, AmountOfSanctions));
                    }
                }
            }

            return newLogs;
        }

        public static void AddLogs(string lastName, string name, string middleName,
            string dateOfVisit, string companyName, string amountOfSanctions)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(@"C:\Users\User\source\repos\OOP_Labs\Лабораторная работа 2\Lib_Lab2\XML.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            // Создаем новый элемент DogCenter.
            XmlElement InspectorsLogElem = xDoc.CreateElement("InspectorsLog");
            // Создаем атрибут LastName.
            XmlAttribute nameAttr = xDoc.CreateAttribute("LastName");
            // Создаем элементы name, middleName, dateOfVisit, companyName и amountOfSanctions.
            XmlElement nameElem = xDoc.CreateElement("name");
            XmlElement middleNameElem = xDoc.CreateElement("middleName");
            XmlElement dateOfVisitElem = xDoc.CreateElement("dateOfVisit");
            XmlElement companyNameElem = xDoc.CreateElement("companyName");
            XmlElement amountOfSanctionsElem = xDoc.CreateElement("amountOfSanctions");

            // Создаем текстовые значения для элементов и атрибута.
            XmlText nameText = xDoc.CreateTextNode(name);
            XmlText middleNameText = xDoc.CreateTextNode(middleName);
            XmlText dateOfVisitText = xDoc.CreateTextNode(dateOfVisit);
            XmlText companyNameText = xDoc.CreateTextNode(companyName);
            XmlText amountOfSanctionsText = xDoc.CreateTextNode(amountOfSanctions);
            XmlText LastNameText = xDoc.CreateTextNode(lastName);

            //добавляем узлы
            nameAttr.AppendChild(LastNameText);
            nameElem.AppendChild(nameText);
            middleNameElem.AppendChild(middleNameText);
            dateOfVisitElem.AppendChild(dateOfVisitText);
            companyNameElem.AppendChild(companyNameText);
            amountOfSanctionsElem.AppendChild(amountOfSanctionsText);
            InspectorsLogElem.Attributes.Append(nameAttr);
            InspectorsLogElem.AppendChild(nameElem);
            InspectorsLogElem.AppendChild(middleNameElem);
            InspectorsLogElem.AppendChild(dateOfVisitElem);
            InspectorsLogElem.AppendChild(companyNameElem);
            InspectorsLogElem.AppendChild(amountOfSanctionsElem);
            xRoot.AppendChild(InspectorsLogElem);
            xDoc.Save(@"C:\Users\User\source\repos\OOP_Labs\Лабораторная работа 2\Lib_Lab2\XML.xml");
        }
    }
}
