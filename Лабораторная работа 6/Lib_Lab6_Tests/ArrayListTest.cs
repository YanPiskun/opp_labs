using System;
using Xunit;
using Lib_Lab6;
using Lib_Lab2;

namespace Lib_Lab6_Tests
{
    public class ArrayListTest
    {
        [Fact]
        public void ArrayListTestInt1()
        {
            //arrenge
            ArrayList<int> TestArray = new ArrayList<int>();
            int FirstValue = 32;
            int SecondValue = 15;
            int expected = 2;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestInt2()
        {
            //arrenge
            ArrayList<int> TestArray = new ArrayList<int>();
            int FirstValue = 32;
            int SecondValue = 15;
            int ThirdValue = -16;
            int expected = 4;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            TestArray.AddNewElement(ThirdValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestInt3()
        {
            //arrenge
            ArrayList<int> TestArray = new ArrayList<int>();
            int FirstValue = 32;
            int SecondValue = 15;
            int ThirdValue = -16;
            int FourthValue = -59;
            int FifthValue = 189;
            int expected = 8;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            TestArray.AddNewElement(ThirdValue);
            TestArray.AddNewElement(FourthValue);
            TestArray.AddNewElement(FifthValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestString1()
        {
            //arrenge
            ArrayList<string> TestArray = new ArrayList<string>();
            string FirstValue = "32";
            string SecondValue = "15";
            int expected = 2;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestString2()
        {
            //arrenge
            ArrayList<string> TestArray = new ArrayList<string>();
            string FirstValue = "32";
            string SecondValue = "15";
            string ThirdValue ="-16";
            int expected = 4;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            TestArray.AddNewElement(ThirdValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestString3()
        {
            //arrenge
            ArrayList<string> TestArray = new ArrayList<string>();
            string FirstValue = "32";
            string SecondValue = "15";
            string ThirdValue = "-16";
            string FourthValue = "-59";
            string FifthValue = "189";
            int expected = 8;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            TestArray.AddNewElement(ThirdValue);
            TestArray.AddNewElement(FourthValue);
            TestArray.AddNewElement(FifthValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestChar1()
        {
            //arrenge
            ArrayList<Char> TestArray = new ArrayList<Char>();
            Char FirstValue = '3';
            Char SecondValue = '1';
            int expected = 2;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestChar2()
        {
            //arrenge
            ArrayList<Char> TestArray = new ArrayList<Char>();
            Char FirstValue = '2';
            Char SecondValue = '5';
            Char ThirdValue = '6';
            int expected = 4;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            TestArray.AddNewElement(ThirdValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestChar3()
        {
            //arrenge
            ArrayList<Char> TestArray = new ArrayList<Char>();
            Char FirstValue = '2';
            Char SecondValue = '5';
            Char ThirdValue = '6';
            Char FourthValue = '9';
            Char FifthValue = '8';
            int expected = 8;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            TestArray.AddNewElement(ThirdValue);
            TestArray.AddNewElement(FourthValue);
            TestArray.AddNewElement(FifthValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestInspectorsLog1()
        {
            //arrenge
            ArrayList<InspectorsLog> TestArray = new ArrayList<InspectorsLog>();
            string Date = "2019-12-12";
            InspectorsLog FirstValue = new InspectorsLog("������", "����", "��������", DateTime.Parse(Date), "���", 16);
            InspectorsLog SecondValue = new InspectorsLog("������", "����", "��������", DateTime.Parse(Date), "���", 16);
            int expected = 2;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArrayListTestInspectorsLog2()
        {
            //arrenge
            ArrayList<InspectorsLog> TestArray = new ArrayList<InspectorsLog>();
            string Date = "2019-12-12";
            InspectorsLog FirstValue = new InspectorsLog("������", "����", "��������", DateTime.Parse(Date), "���", 16);
            InspectorsLog SecondValue = new InspectorsLog("������", "����", "��������", DateTime.Parse(Date), "���", 16);
            InspectorsLog ThirdValue = new InspectorsLog("������", "����", "��������", DateTime.Parse(Date), "���", 16);
            int expected = 4;
            //act
            TestArray.AddNewElement(FirstValue);
            TestArray.AddNewElement(SecondValue);
            TestArray.AddNewElement(ThirdValue);
            int actual = TestArray.Length();
            //assert
            Assert.Equal(expected, actual);
        }
    }
}
