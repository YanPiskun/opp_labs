﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab6
{
    public class ArrayList<T>
    {
        public T[] Array { get; set; }
        public int CurrentElement {get; set;}

        public ArrayList()
        {
            Array = new T[1];
            CurrentElement = 0;
        }

        public void AddNewElement(T element)
        {
            IsFill();
            Array[CurrentElement] = element;
            CurrentElement++;
        }

        void IsFill()
        {
            if(Array.Length == CurrentElement)
            {
                T[] TemperaryArray = Array;
                Array = new T[Array.Length * 2];
                CurrentElement = 0;
                
                foreach(T elem in TemperaryArray)
                {
                    AddNewElement(elem);
                }
            }
        }

        public int Length()
        {
            return Array.Length;
        }

        public List<T> GetList()
        {
            return Array.ToList<T>();
        }
    }
}
