﻿using System.Collections;
using System.Collections.Generic;

namespace Lib_Lab6
{
    public class Iterator : IEnumerable
    {
        List<string> PrintArray;
        public Iterator(List<string> printTree)
        {
            this.PrintArray = printTree;
        }
        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < PrintArray.Count; i++)
            {
                yield return PrintArray[i] + "\n";
            }
        }
    }
}
