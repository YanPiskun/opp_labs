﻿GO
CREATE DATABASE EnergySupervisionWorkersCenter;

GO
USE EnergySupervisionWorkersCenter;

CREATE TABLE Workers ( WorkerID int identity(1, 1) not null primary key,
LastName varchar(30), Name varchar(30), MiddleName varchar(30));

CREATE TABLE Factories ( FactoryID int identity(1, 1) not null primary key,
CompanyName varchar(30));

CREATE TABLE Verifications ( VerificationID int identity(1, 1) not null primary key,
DateOfVerification Date, AmountOfSanctions int,
WorkerID int foreign key references Workers(WorkerID),
FactoryID int foreign key references Factories(FactoryID));