﻿using System;
using System.Linq;
using System.Data.Linq;
using System.Collections.Generic;

namespace Lib_Lab8
{
    public class DataBaseConnection
    {
        public string ConnectionString;

        public DataBaseConnection(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public List<Factory> GetInfoFactories()
        {
            List<Factory> Factories = new List<Factory>();

            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                Table<Factory> DataBaseFactories = DataBase.GetTable<Factory>();

                foreach (Factory factory in DataBaseFactories)
                {
                    Factories.Add(factory);
                }
            }

            return Factories;
        }

        public List<Worker> GetInfoWorkers()
        {
            List<Worker> Workers = new List<Worker>();

            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                Table<Worker> DataBaseWorkers = DataBase.GetTable<Worker>();

                foreach (Worker worker in DataBaseWorkers)
                {
                    Workers.Add(worker);
                }
            }

            return Workers;
        }

        public List<Verification> GetInfoVerifications()
        {
            List<Verification> Verifications = new List<Verification>();

            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                Table<Verification> DataBaseVerifications = DataBase.GetTable<Verification>();

                foreach (Verification verification in DataBaseVerifications)
                {
                    Verifications.Add(verification);
                }
            }

            return Verifications;
        }

        public void DeleteVerification(int VerificationID)
        {
            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                var DeleteVerification = (from u in DataBase.GetTable<Verification>()
                                 where u.VerificationID.ToString() == VerificationID.ToString()
                                 select u).FirstOrDefault();

                DataBase.GetTable<Verification>().DeleteOnSubmit(DeleteVerification);
                DataBase.SubmitChanges();
            }
        }

        public void UpdateVerification(int VerificationID, int AmountOfSanctions)
        {
            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                var UpdateVerification = (from u in DataBase.GetTable<Verification>()
                                 where u.VerificationID.ToString() == VerificationID.ToString()
                                 select u).FirstOrDefault();

                UpdateVerification.AmountOfSanctions = AmountOfSanctions;
                DataBase.SubmitChanges();
            }
        }

        public void AddNewVerification(DateTime DateOfVerification,
            int AmountOfSanctions, int WorkerID, int FactoryID)
        {
            using (DataContext DataBase = new DataContext(ConnectionString))
            {
                Verification NewVerification = new Verification() { DateOfVerification = DateOfVerification, AmountOfSanctions = AmountOfSanctions,
                WorkerID = WorkerID, FactoryID = FactoryID};

                DataBase.GetTable<Verification>().InsertOnSubmit(NewVerification);
                DataBase.SubmitChanges();
            }
        }
    }
}
