﻿using System.Data.Linq.Mapping;

namespace Lib_Lab8
{
    [Table(Name = "Workers")]
    public class Worker
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int WorkerID { get; set; }
        [Column(Name = "LastName")]
        public string LastName { get; set; }
        [Column(Name = "Name")]
        public string Name { get; set; }
        [Column(Name = "MiddleName")]
        public string MiddleName { get; set; }

        public override string ToString()
        {
            return LastName + " " + Name + " " + MiddleName;
        }
    }
}
