﻿using System.Data.Linq.Mapping;

namespace Lib_Lab8
{
    [Table(Name = "Factories")]
    public class Factory
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int FactoryID { get; set; }
        [Column(Name = "CompanyName")]
        public string CompanyName { get; set; }

        public override string ToString()
        {
            return CompanyName;
        }
    }
}
