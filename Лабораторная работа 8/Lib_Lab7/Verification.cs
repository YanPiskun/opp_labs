﻿using System;
using System.Data.Linq.Mapping;

namespace Lib_Lab8
{
    [Table(Name = "Verifications")]
    public class Verification
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int VerificationID { get; set; }
        [Column(Name = "DateOfVerification")]
        public DateTime DateOfVerification { get; set; }
        [Column(Name = "AmountOfSanctions")]
        public int AmountOfSanctions { get; set; }
        [Column(Name = "WorkerID")]
        public int WorkerID { get; set; }
        [Column(Name = "FactoryID")]
        public int FactoryID { get; set; }


        public override string ToString()
        {
            return "Проверка №" + VerificationID.ToString();
        }
    }
}
