﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lib_Lab8;

namespace Lab8_App
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataBaseConnection connection = new DataBaseConnection("Data Source=DESKTOP-1T99363;Initial Catalog=EnergySupervisionWorkersCenter;Integrated Security=True");
        List<Factory> Factories = new List<Factory>();
        List<Verification> Verifications = new List<Verification>();
        List<Worker> Workers = new List<Worker>();

        public MainWindow()
        {
            InitializeComponent();
            ConversionWorkers();
            ConversionFactories();
            ConversionVerifications();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox1.Text, out _))
            {
                connection.DeleteVerification(int.Parse(textBox1.Text));
                ConversionVerifications();
            }

            textBox1.Text = "";
        }

        private void ConversionWorkers()
        {
            Workers = new List<Worker>();
            Workers = connection.GetInfoWorkers();
            comboBox1.Items.Clear();

            foreach (Worker worker in Workers)
            {
                comboBox1.Items.Add(worker);
            }
        }

        private void ConversionFactories()
        {
            Factories = new List<Factory>();
            Factories = connection.GetInfoFactories();
            comboBox2.Items.Clear();

            foreach (Factory factory in Factories)
            {
                comboBox2.Items.Add(factory);
            }
        }

        private void ConversionVerifications()
        {
            Verifications = new List<Verification>();
            Verifications = connection.GetInfoVerifications();
            listBox1.Items.Clear();

            foreach (Verification verification in Verifications)
            {
                listBox1.Items.Add(verification);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox1.Text, out _) && int.TryParse(textBox2.Text, out _))
            {
                connection.UpdateVerification(int.Parse(textBox1.Text), int.Parse(textBox2.Text));
                ConversionVerifications();
            }

            textBox1.Text = textBox2.Text = "";
        }

        private void listBox1_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                label1.Content = Verifications[listBox1.SelectedIndex].VerificationID.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (DateTime.TryParse(textBox4.Text, out _) && int.TryParse(textBox5.Text, out _) &&
                comboBox1.SelectedIndex >= 0 && comboBox1.SelectedIndex < comboBox1.Items.Count &&
                comboBox2.SelectedIndex >= 0 && comboBox2.SelectedIndex < comboBox2.Items.Count)
            {
                connection.AddNewVerification(DateTime.Parse(textBox4.Text), int.Parse(textBox5.Text),
                    Workers[comboBox1.SelectedIndex].WorkerID, Factories[comboBox2.SelectedIndex].FactoryID);

                ConversionVerifications();
            }
            else
            {
                MessageBox.Show("Данные введены неверно!");
            }

            textBox5.Text = textBox4.Text = "";
        }
    }
}
