﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Lab9_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.AddRange(System.IO.File.ReadAllText(@"C:\Users\User\source\repos\OOP_Labs\Лабораторная работа 9\Lab9.txt").Split(' ', '\n', '\r')
                .Select(x => { string y = ""; for (int i = 0; i < x.Length; i++) { if (char.IsLetter(x[i])) { y += x[i]; } }; return y; })
                .Select(word => word.ToLower()).Where(word => word.Length >= 4).ToArray());
        }
    }
}
