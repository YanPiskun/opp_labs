﻿namespace WindowsFormsLab3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Read = new System.Windows.Forms.Button();
            this.Write = new System.Windows.Forms.Button();
            this.ReadBox = new System.Windows.Forms.RichTextBox();
            this.WriteBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Read
            // 
            this.Read.Location = new System.Drawing.Point(654, 213);
            this.Read.Name = "Read";
            this.Read.Size = new System.Drawing.Size(134, 49);
            this.Read.TabIndex = 0;
            this.Read.Text = "Чтение";
            this.Read.UseVisualStyleBackColor = true;
            this.Read.Click += new System.EventHandler(this.Read_Click);
            // 
            // Write
            // 
            this.Write.Location = new System.Drawing.Point(654, 26);
            this.Write.Name = "Write";
            this.Write.Size = new System.Drawing.Size(134, 54);
            this.Write.TabIndex = 1;
            this.Write.Text = "Запись";
            this.Write.UseVisualStyleBackColor = true;
            this.Write.Click += new System.EventHandler(this.Write_Click);
            // 
            // ReadBox
            // 
            this.ReadBox.Location = new System.Drawing.Point(12, 213);
            this.ReadBox.Name = "ReadBox";
            this.ReadBox.ReadOnly = true;
            this.ReadBox.Size = new System.Drawing.Size(587, 155);
            this.ReadBox.TabIndex = 4;
            this.ReadBox.Text = "";
            // 
            // WriteBox
            // 
            this.WriteBox.Location = new System.Drawing.Point(12, 26);
            this.WriteBox.Name = "WriteBox";
            this.WriteBox.Size = new System.Drawing.Size(587, 157);
            this.WriteBox.TabIndex = 5;
            this.WriteBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.WriteBox);
            this.Controls.Add(this.ReadBox);
            this.Controls.Add(this.Write);
            this.Controls.Add(this.Read);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Read;
        private System.Windows.Forms.Button Write;
        private System.Windows.Forms.RichTextBox ReadBox;
        private System.Windows.Forms.RichTextBox WriteBox;
    }
}

