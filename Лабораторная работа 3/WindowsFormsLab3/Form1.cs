﻿using System;
using System.IO;
using System.Windows.Forms;
using TimeOut;

namespace WindowsFormsLab3
{
    public partial class Form1 : Form
    {
        Stream stream;
        public Form1()
        {
            InitializeComponent();
            stream = new MemoryStream();
            stream = new Timeout(stream);
        }


        private void Write_Click(object sender, EventArgs e)
        {
            stream.Seek(0, SeekOrigin.Begin);
            var text = WriteBox.Text;
            byte[] s = System.Text.Encoding.UTF8.GetBytes(text);
            stream.Write(s, 0, s.Length);
        }

        private void Read_Click(object sender, EventArgs e)
        {
            try
            {
                stream.Seek(0, SeekOrigin.Begin);
                byte[] s = new byte[stream.Length];
                stream.Read(s, 0, s.Length);
                string str = System.Text.Encoding.UTF8.GetString(s);
                ReadBox.Text = str;
            }
            catch
            {
                MessageBox.Show("Timeout");
            }

        }
    }
}
