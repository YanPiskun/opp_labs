﻿using System;
using System.IO;

namespace TimeOut
{
    public class Timeout : Stream
    {
        public int TimeoutToRead { get; set; }
        Stream Stream { get; set; }

        public Timeout(Stream stream)
        {
            this.Stream = stream;
            TimeoutToRead = 3000;
        }

        private DateTime time;

        public override int Read(byte[] buffer, int offset, int count)
        {
            var interval = (DateTime.Now - time).TotalMilliseconds;

            if (interval < TimeoutToRead)
                return Stream.Read(buffer, offset, count);

            throw new TimeoutException();
        }

        public override bool CanRead
        {
            get { return Stream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return Stream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return Stream.CanWrite; }
        }

        public override void Flush()
        {
            Stream.Flush();
        }

        public override long Length
        {
            get { return Stream.Length; }
        }

        public override long Position
        {
            get { return Stream.Position; }
            set { Stream.Position = value; }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return Stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            Stream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            Stream.Write(buffer, offset, count);
            time = DateTime.Now;
        }
    }
}