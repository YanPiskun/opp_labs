﻿using System;
using Lab1;


namespace ConsoleLab1
{
    class Program
    {
        static Matrix Input()
        {
            Console.WriteLine("Input rows count");
            int row, col;
            while (!int.TryParse(Console.ReadLine(), out row))
            {
                Console.WriteLine("Incorrect input!");
            }
            Console.WriteLine("Input columns count");
            while (!int.TryParse(Console.ReadLine(), out col))
            {
                Console.WriteLine("Incorrect input!");
            }
            float[,] tmp = new float[row, col];
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    float tmpNum;
                    Console.WriteLine($"Input Matrix[{i}, {j}]");
                    while (!float.TryParse(Console.ReadLine(), out tmpNum))
                    {
                        Console.WriteLine("Incorrect input!");
                    }
                    tmp[i, j] = tmpNum;
                }
            }
            Matrix m = new Matrix(tmp);
            return m;
        }

        static void Main(string[] args)
        {
            
            Console.WriteLine("Input matrix A:");
            Matrix A = Input();

            Console.WriteLine($"Matrix A:\n{A.ToString()}");
            A.LDLT(out float[,] L, out float[] D);
            Console.WriteLine(new Matrix(L).ToString());
            foreach (float n in D)
            {
                Console.WriteLine(n);
            }
            Console.ReadKey();
        }
    }
}
