﻿using System;
using System.Collections.Generic;

namespace Lab1
{
    public class Matrix
    {
        /// <summary>
        /// Содержимое матрицы
        /// </summary>
        public float[,] Data { get; private set; }
        /// <summary>
        /// Строки матрицы
        /// </summary>
        public int Rows { get { return Data.GetLength(0); } }
        /// <summary>
        /// Колонки матрицы
        /// </summary>
        public int Columns { get { return Data.GetLength(1); } }

        /// <summary>
        /// Конструктор, который инициализирует размерность матрицы
        /// </summary>
        /// <param name="m">Количество строк в матрице.</param>
        /// <param name="n">Количество столбцов в матрице.</param>
        public Matrix(int m, int n)
        {
            Data = new float[m, n];
        }

        public float this[int i, int j]
        {
            get
            {
                return Data[i, j];
            }
            set
            {
                Data[i, j] = value;
            }
        }

        /// <summary>
        /// Конструктор, который инициализирует матрицу
        /// </summary>
        /// <param name="arr">Объект, содержащий в себе матрицу и её размерность.</param>
        public Matrix(float[,] arr)
        {
            Data = new float[arr.GetLength(0), arr.GetLength(1)];
            Data = (float[,])arr.Clone();
        }


        public void LDLT(out float[,] L, out float[] D)
        {

            if (Rows != Columns)
            {
                throw new Exception("The number of rows and columns is not equal!");
            }
            IsMatrixSymmetricalTest();
            int n = Data.GetLength(0);
            D = new float[n];
            L = new float[n, n];
            int i, j, k;
            float sum = 0;
            for (i = 0; i < n; i++)//столбцы
            {
                for (j = i; j < n; j++)//строки
                {
                    sum = Data[j, i];//значение вычисляемого элемента
                    for (k = 0; k < i; k++)//вычитание элементов строки из вычисляемого элемента
                        sum = sum - L[i, k] * D[k] * L[j, k];
                    if (i == j)//диагональный элемент
                    {
                        if (sum <= 0) new Exception("A is not positive deﬁnite!");
                        D[i] = sum;//диагональный элемент
                        L[i, i] = 1;//диагональ
                    }
                    else L[j, i] = sum / D[i];//внедиагональный элемент
                }
            }
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.Columns != m2.Rows)
            {
                throw new Exception("It is impossible to multiply matrices. The number of rows and columns does not match.");
            }
            int m = m1.Rows;
            int n = m2.Columns;
            float[,] newMatrix = new float[m, n];
            for (int i = 0; i < m1.Rows; i++)
            {
                for (int j = 0; j < m2.Columns; j++)
                {
                    for (int k = 0; k < m2.Rows; k++)
                    {
                        newMatrix[i, j] += m1.Data[i, k] * m2.Data[k, j];
                    }
                }
            }
            return new Matrix(newMatrix);
        }

        /// <summary>
        /// Умножение вектора на матрицу
        /// </summary>
        /// <param name="arr">Объект, содержащий в себе вектор.</param>
        /// <param name="m1">Объект, содержащий в себе матрицу и её размерность.</param>
        /// <returns></returns>
        public static Matrix operator *(float[] arr, Matrix m1)
        {
            var m = new Matrix(1, arr.Length);
            for (int i = 0; i < arr.Length; i++)
            {
                m.Data[0, i] = arr[i];
            }
            return m * m1;
        }

        /// <summary>
        /// Умножение матрицы на вектора
        /// </summary>
        /// <param name="m1">Объект, содержащий в себе матрицу и её размерность.</param>
        /// <param name="arr">Объект, содержащий в себе вектор.</param>
        /// <returns></returns>
        public static Matrix operator *(Matrix m1, float[] arr)
        {
            var m = new Matrix(arr.Length, 1);
            for (int i = 0; i < arr.Length; i++)
            {
                m.Data[i, 0] = arr[i];
            }
            return m1 * m;
        }

        private void IsMatrixSymmetricalTest()
        {
            for (int i = 0; i < Data.GetLength(0); i++)
            {
                for (int j = 0; j < Data.GetLength(1); j++)
                {
                    if (i != j && Data[i, j] != Data[j, i])
                    {
                        throw new Exception("This matrix isn't symmetrical");
                    }
                }
            }
        }

        /// <summary>
        /// Вывод матрицы
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    str += $"{Data[i, j]}\t";
                }
                str += '\n';
            }
            return str;
        }

        public override bool Equals(object obj)
        {
            var matrix = obj as Matrix;
            if (matrix != null)
            {
                if (this.Columns == matrix.Columns && this.Rows == matrix.Rows)
                {
                    for (int i = 0; i < this.Rows; i++)
                    {
                        for (int j = 0; j < this.Columns; j++)
                        {
                            if (this[i, j] != matrix[i, j])
                            {
                                return false;
                            }
                            
                        }
                    }
                    return true;
                }
            }
            return false;   
        }
    }
}
