﻿namespace Lib_Lab4
{
    public class Points
    {
        private double X;
        private double Y;
        public Points(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public double GetX()
        {
            return X;
        }

        public double GetY()
        {
            return Y;
        }
    }
}
