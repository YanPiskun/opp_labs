﻿using System.IO;
using System;

namespace Lib_Lab4
{
    public delegate double Function(double X);
    public class PointForPictures
    {
        public Function Operation;
        public Action<double, double> SendCoord { get; set; }
        string path = @"C:\Users\User\source\repos\OOP_Labs\Лабораторная работа 4\Points.txt";

        public PointForPictures(Function operation)
        {
            Operation = operation;
        }

        public void GetPoints()
        {
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    String[] Points = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    if (Double.TryParse(Points[0], out _) && Double.TryParse(Points[1], out _))
                    {
                        SendCoord(double.Parse(Points[0]), double.Parse(Points[1]));
                    }
                }
            }
        }

        public void FillingValues(int length)
        {
            using (StreamWriter writeRandomValue = new StreamWriter(path, false, System.Text.Encoding.Default))
            {
                for (int i = 0; i < length; i += 5)
                {
                    writeRandomValue.WriteLine(i.ToString() + " " + Operation(i));
                }
            }
        }
    }
}
