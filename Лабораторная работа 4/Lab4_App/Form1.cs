﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Lib_Lab4;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lab4_App
{
    public partial class Form1 : Form
    {
        Button Move = new Button();
        RadioButton LineRadioButton = new RadioButton();
        RadioButton SqrtRadioButton = new RadioButton();
        RadioButton CenterLineRadioButton = new RadioButton();
        PointForPictures Points;
        List<Points> PointsForDrawing;
        Graphics Path;
        Graphics MoveObject;

        public Form1()
        {
            InitializeComponent();

            Path = Graphic.CreateGraphics();
            MoveObject = Graphic.CreateGraphics();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Размер окна и название окна.
            this.Size = new System.Drawing.Size(1056, 424);
            this.Text = "Lab4";

            LineRadioButton.Location = new Point(882, 161);
            LineRadioButton.Text = "y = X / 2.3";
            this.Controls.Add(LineRadioButton);

            SqrtRadioButton.Location = new Point(882, 186);
            SqrtRadioButton.Size = new Size(150, 25);
            SqrtRadioButton.Text = "y = 10 * sqrt(X)";
            this.Controls.Add(SqrtRadioButton);

            CenterLineRadioButton.Location = new Point(882, 211);
            CenterLineRadioButton.Text = "По центру";
            this.Controls.Add(CenterLineRadioButton);

            Button CreatePath = new Button();
            CreatePath.Location = new Point(882, 12);
            CreatePath.Size = new Size(144, 44);
            CreatePath.Text = "Вывести путь";
            CreatePath.Click += (o, a) => CreatePathEvent(o, a);
            this.Controls.Add(CreatePath);

            Move.Location = new Point(882, 73);
            Move.Size = new Size(144, 44);
            Move.Text = "Движение";
            Move.Click += MoveEvent;
            this.Controls.Add(Move);
            Move.Visible = false;
        }

        private void CreatePathEvent(object sender, EventArgs e)
        {
            bool IsChoice = false;

            if(LineRadioButton.Checked == true)
            {
                Points = new PointForPictures(X => X/2.28);
                IsChoice = true;
            }
            else if(SqrtRadioButton.Checked == true)
            {
                Points = new PointForPictures(X => 10 * Math.Sqrt(X));
                IsChoice = true;
            }
            else if(CenterLineRadioButton.Checked == true)
            {
                Points = new PointForPictures(X => Graphic.Height / 2);
                IsChoice = true;
            }
            else
            {
                MessageBox.Show("Не выбрана функция!");
            }

            if(IsChoice)
            {
                Move.Visible = true;

                Path.Clear(Color.White);
                PointsForDrawing = new List<Points>();

                Points.FillingValues(Graphic.Width);
                Points.SendCoord = AddNewPoint;
                Points.GetPoints();

                DrawPath(PointsForDrawing);
            }
        }

        private void AddNewPoint(double X, double Y)
        {
            PointsForDrawing.Add(new Points(X, Graphic.Height - Y));
        }

        private void DrawPath(List<Points> PointsForDrawing)
        {
            for (int i = 0; i < PointsForDrawing.Count; i++)
            {
                if (i != 0)
                {
                    Path.DrawLine(
                    new Pen(Color.Red, 2f),
                    new Point((int)PointsForDrawing[i - 1].GetX(), (int)PointsForDrawing[i - 1].GetY()),
                    new Point((int)PointsForDrawing[i].GetX(), (int)PointsForDrawing[i].GetY()));
                }
            }
        }

        private async void MoveEvent(object sender, EventArgs e)
        {
            for (int i = 0; i <= PointsForDrawing.Count; i++)
            {
                if (i != 0)
                {
                    Path.Clear(Color.White);
                    DrawPath(PointsForDrawing);
                    DrawStar(PointsForDrawing[i - 1].GetX(), PointsForDrawing[i - 1].GetY(), 5);
                    
                    await Task.Delay(50);
                }
            }
        }

        private void DrawStar(double CenterX, double CenterY, int AmountOfPeaks)
        {
            PointF[] points = new PointF[2 * AmountOfPeaks + 1];
            double R = 10, r = 20;   // радиусы
            double alpha = 0;        // поворот
            double a = alpha, da = Math.PI / AmountOfPeaks, l;
            for (int k = 0; k < 2 * AmountOfPeaks + 1; k++)
            {
                l = k % 2 == 0 ? r : R;
                points[k] = new PointF((float)(CenterX + l * Math.Cos(a)), (float)(CenterY + l * Math.Sin(a)));
                a += da;
            }

            MoveObject.DrawLines(Pens.Blue, points);
        }
    }
}
