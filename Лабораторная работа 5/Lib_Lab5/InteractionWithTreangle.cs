﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab5
{
    public class InteractionWithTreangle
    {
        public List<Treangle> Treangles = new List<Treangle>();
        public List<int> NumbersForInfoLabel;
        public event EventHandler<EventGetResult> GetResult;

        public void AddNewTreangle(int centerX, int centerY, int radius)
        {
            Treangles.Add(new Treangle(centerX, centerY, radius));
        }

        public void CollisionCheckedClick(int pointX, int pointY)
        {
            foreach(Treangle treangle in Treangles)
            {
                treangle.CollisionDetectionForClick(pointX, pointY);
            }
        }

        public void DeleteTreangleClick(int pointX, int pointY)
        {
            for (int i = 0; i < Treangles.Count; i++)
            {
                int AmountOfClick = Treangles[i].AmountOfClick;
                Treangles[i].CollisionDetectionForClick(pointX, pointY);
                if (Treangles[i].AmountOfClick - AmountOfClick > 0)
                {
                    Treangles.Remove(Treangles[i]);
                }
            }
        }

        public void CollisionCheckedMove(int pointX, int pointY)
        {
            NumbersForInfoLabel = new List<int>();

            for (int i = 0; i < Treangles.Count; i++)
            {
                if (Treangles[i].CollisionDetectionForMove(pointX, pointY))
                {
                    NumbersForInfoLabel.Add(i);
                }
            }

            if(NumbersForInfoLabel.Count > 0)
            {
                if (GetResult != null) GetResult(this, new EventGetResult(true));
            }
            else
            {
                if (GetResult != null) GetResult(this, new EventGetResult(false));
            }
        }
    }
}
