﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab5
{
    public class EventGetResult : EventArgs
    {
        public bool Result { get; private set; }
        public EventGetResult(bool result)
        {
            Result = result;
        }
    }
}
