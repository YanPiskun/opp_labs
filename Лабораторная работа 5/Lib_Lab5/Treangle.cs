﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab5
{
    public class Treangle
    {
        static int Number = 1;

        string Name;
        public Point CenterCoord { get; set; }
        public int Radius { get; set; }
        public int AmountOfClick { get; set; }
        public Point FirstVertex { get; set; }
        public Point SecondVertex { get; set; }
        public Point ThirdVertex { get; set; }

        public Treangle(int centerX, int centerY, int radius)
        {
            CenterCoord = new Point(centerX, centerY);
            Radius = radius;
            AmountOfClick = 0;
            Name = Number.ToString();
            Number++;

            FirstVertex = new Point(CenterCoord.X - radius * Math.Cos(30 * Math.PI / 180), 
                CenterCoord.Y - radius * Math.Cos(60 * Math.PI / 180));
            SecondVertex = new Point(CenterCoord.X, CenterCoord.Y + Radius);
            ThirdVertex = new Point(CenterCoord.X + radius * Math.Cos(30 * Math.PI / 180),
                CenterCoord.Y - radius * Math.Cos(60 * Math.PI / 180));
        }

        public void CollisionDetectionForClick(int PointX, int PointY)
        { 
            double a = (FirstVertex.X - PointX) * (SecondVertex.Y - FirstVertex.Y) - (SecondVertex.X - FirstVertex.X) * (FirstVertex.Y - PointY);
            double b = (SecondVertex.X - PointX) * (ThirdVertex.Y - SecondVertex.Y) - (ThirdVertex.X - SecondVertex.X) * (SecondVertex.Y - PointY);
            double c = (ThirdVertex.X - PointX) * (FirstVertex.Y - ThirdVertex.Y) - (FirstVertex.X - ThirdVertex.X) * (ThirdVertex.Y - PointY);

            if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0))
            {
                AmountOfClick++;
            }
        }

        public override string ToString()
        {
            return "Кликов по №" + Name + " - " + AmountOfClick.ToString();
        } 

        public bool CollisionDetectionForMove(int PointX, int PointY)
        {
            double a = (FirstVertex.X - PointX) * (SecondVertex.Y - FirstVertex.Y) - (SecondVertex.X - FirstVertex.X) * (FirstVertex.Y - PointY);
            double b = (SecondVertex.X - PointX) * (ThirdVertex.Y - SecondVertex.Y) - (ThirdVertex.X - SecondVertex.X) * (SecondVertex.Y - PointY);
            double c = (ThirdVertex.X - PointX) * (FirstVertex.Y - ThirdVertex.Y) - (FirstVertex.X - ThirdVertex.X) * (ThirdVertex.Y - PointY);

            if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0))
            {
                return true;
            }

            return false;
        }
    }
}
