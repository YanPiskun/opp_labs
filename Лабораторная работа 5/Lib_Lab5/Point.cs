﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab5
{
    public class Point
    {
        public double X;
        public double Y;
        public Point(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }
}
