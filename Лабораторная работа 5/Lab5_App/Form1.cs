﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Lib_Lab5;

namespace Lab5_App
{
    public partial class Form1 : Form
    {
        InteractionWithTreangle interactionWithTreangle;
        Graphics drawTreangles;
        public Form1()
        {
            InitializeComponent();
            interactionWithTreangle = new InteractionWithTreangle();
            drawTreangles = pictureBox1.CreateGraphics();

            interactionWithTreangle.GetResult += InteractionWithTreangle_GetResult;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.MouseClick += PictureBox1_MouseClick;
            pictureBox1.MouseDoubleClick += PictureBox1_MouseDoubleClick;
            pictureBox1.MouseMove += PictureBox1_MouseMove;
        }

        private void PictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                interactionWithTreangle.AddNewTreangle(e.X, e.Y, 20);
                DrawTreangles();
            }
            else if (radioButton2.Checked == true)
            {
                interactionWithTreangle.DeleteTreangleClick(e.X, e.Y);
                DrawTreangles();
            }
        }

        private void InteractionWithTreangle_GetResult(object sender, EventGetResult e)
        {
            if(e.Result)
            {
                label1.Text = "";
                for (int i = 0; i < interactionWithTreangle.NumbersForInfoLabel.Count; i++)
                {
                    int Number = interactionWithTreangle.NumbersForInfoLabel[i];
                    label1.Text += interactionWithTreangle.Treangles[Number].ToString() + "\n";
                }
            }
            else if(!e.Result)
            {
                label1.Text = "";
            }
        }

        private void PictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            interactionWithTreangle.CollisionCheckedClick(e.X, e.Y);
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            interactionWithTreangle.CollisionCheckedMove(e.X, e.Y);
        }

        private void DrawTreangles()
        {
            drawTreangles.Clear(Color.White);

            foreach(Treangle treangle in interactionWithTreangle.Treangles)
            {
                drawTreangles.DrawLine(
                new Pen(Color.Blue, 2f),
                new System.Drawing.Point((int)treangle.FirstVertex.X, (int)treangle.FirstVertex.Y),
                new System.Drawing.Point((int)treangle.SecondVertex.X, (int)treangle.SecondVertex.Y));

                drawTreangles.DrawLine(
                new Pen(Color.Blue, 2f),
                new System.Drawing.Point((int)treangle.SecondVertex.X, (int)treangle.SecondVertex.Y),
                new System.Drawing.Point((int)treangle.ThirdVertex.X, (int)treangle.ThirdVertex.Y));

                drawTreangles.DrawLine(
                new Pen(Color.Blue, 2f),
                new System.Drawing.Point((int)treangle.ThirdVertex.X, (int)treangle.ThirdVertex.Y),
                new System.Drawing.Point((int)treangle.FirstVertex.X, (int)treangle.FirstVertex.Y));
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
