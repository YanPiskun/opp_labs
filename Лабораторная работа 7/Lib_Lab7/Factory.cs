﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab7
{
    public class Factory
    {
        public int FactoryID { get; set; }
        public string CompanyName { get; set; }

        public Factory(int factoryID, string companyName)
        {
            FactoryID = factoryID;
            CompanyName = companyName;
        }

        public override string ToString()
        {
            return CompanyName;
        }
    }
}
