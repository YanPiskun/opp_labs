﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_Lab7
{
    public class Verification
    {
        public int VerificationID { get; set; }
        public DateTime DateOfVerification { get; set; }
        public int AmountOfSanctions { get; set; }
        public int WorkerID { get; set; }
        public int FactoryID { get; set; }

        public Verification(int verificationID, DateTime dateOfVerification,
            int amountOfSanctions, int workerID, int factoryID)
        {
            VerificationID = verificationID;
            DateOfVerification = dateOfVerification;
            AmountOfSanctions = amountOfSanctions;
            WorkerID = workerID;
            FactoryID = factoryID;
        }

        public override string ToString()
        {
            return "Проверка №" + VerificationID.ToString();
        }
    }
}
