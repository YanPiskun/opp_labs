﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Lib_Lab7
{
    public class DataBaseConnection
    {
        public string ConnectionString;

        public DataBaseConnection(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public List<Verification> GetInfoVerification()
        {
            List<Verification> Verifications = new List<Verification>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Verifications", connection);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Verifications.Add(new Verification(reader.GetInt32(0), reader.GetDateTime(1), reader.GetInt32(2),
                            reader.GetInt32(3), reader.GetInt32(4)));
                    }
                }
            }

            return Verifications;
        }

        public List<Worker> GetInfoWorker()
        {
            List<Worker> Workers = new List<Worker>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Workers", connection);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Workers.Add(new Worker(reader.GetInt32(0), reader.GetString(1), reader.GetString(2),
                            reader.GetString(3)));
                    }
                }
            }

            return Workers;
        }

        public List<Factory> GetInfoFactories()
        {
            List<Factory> Factories = new List<Factory>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Factories", connection);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Factories.Add(new Factory(reader.GetInt32(0), reader.GetString(1)));
                    }
                }
            }

            return Factories;
        }

        public void UpdateVerification(int VerificationID, int AmountOfSanctions)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlParameter FirstParam = new SqlParameter("@AmountOfSanctions", AmountOfSanctions);
                SqlParameter SecondParam = new SqlParameter("@VerificationID", VerificationID);
                string SqlExpression = "UPDATE Verifications SET AmountOfSanctions = @AmountOfSanctions" +
                    " WHERE VerificationID = @VerificationID";
                SqlCommand command = new SqlCommand(SqlExpression, connection);
                command.Parameters.Add(FirstParam);
                command.Parameters.Add(SecondParam);
                command.ExecuteNonQuery();
            }
        }

        public void DeleteVerification(int VerificationID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                string sqlExpression = "DELETE FROM Verifications WHERE VerificationID = @VerificationID";
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                // создаем параметр для ID
                SqlParameter IdParam = new SqlParameter("@VerificationID", VerificationID);
                // добавляем параметр к команде
                command.Parameters.Add(IdParam);

                command.ExecuteNonQuery();
            }
        }

        public void AddNewVerification(DateTime DateOfVerification,
            int AmountOfSanctions, int WorkerID, int FactoryID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                // Открытие соединения
                connection.Open();

                SqlParameter dateOfVerification = new SqlParameter("@DateOfVerification", DateOfVerification.ToString());
                SqlParameter amountOfSanctions = new SqlParameter("@AmountOfSanctions", AmountOfSanctions.ToString("d"));
                SqlParameter workerID = new SqlParameter("@WorkerID", WorkerID.ToString());
                SqlParameter factoryID = new SqlParameter("@FactoryID", FactoryID.ToString());

                string sqlExpression = "INSERT INTO Verifications (DateOfVerification, AmountOfSanctions, WorkerID, FactoryID) VALUES " +
                    "(@DateOfVerification, @AmountOfSanctions, @WorkerID, @FactoryID);";
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.Add(dateOfVerification);
                command.Parameters.Add(amountOfSanctions);
                command.Parameters.Add(workerID);
                command.Parameters.Add(factoryID);

                command.ExecuteNonQuery();
            }
        }
    }
}
