﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Lib_Lab7;


namespace Lab7_App
{
    public partial class Form1 : Form
    {
        DataBaseConnection connection = new DataBaseConnection("Data Source=DESKTOP-1T99363;Initial Catalog=EnergySupervisionWorkersCenter;Integrated Security=True");
        List<Factory> Factories = new List<Factory>();
        List<Verification> Verifications = new List<Verification>();
        List<Worker> Workers = new List<Worker>();

        public Form1()
        {
            InitializeComponent();
            ConversionWorkers();
            ConversionFactories();
            ConversionVerifications();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox1.Text, out _))
            {
                connection.DeleteVerification(int.Parse(textBox1.Text));
                ConversionVerifications();
            }

            textBox1.Text = "";
        }

        private void ConversionWorkers()
        {
            Workers = new List<Worker>();
            Workers = connection.GetInfoWorker();
            comboBox3.Items.Clear();

            foreach (Worker worker in Workers)
            {
                comboBox3.Items.Add(worker);
            }
        }

        private void ConversionFactories()
        {
            Factories = new List<Factory>();
            Factories = connection.GetInfoFactories();
            comboBox2.Items.Clear();

            foreach (Factory factory in Factories)
            {
                comboBox2.Items.Add(factory);
            }
        }

        private void ConversionVerifications()
        {
            Verifications = new List<Verification>();
            Verifications = connection.GetInfoVerification();
            listBox1.Items.Clear();

            foreach (Verification verification in Verifications)
            {
                listBox1.Items.Add(verification);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox1.Text, out _) && int.TryParse(textBox2.Text, out _))
            {
                connection.UpdateVerification(int.Parse(textBox1.Text), int.Parse(textBox2.Text));
                ConversionVerifications();
            }

            textBox1.Text = textBox2.Text = "";
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                propertyGrid1.SelectedObject = listBox1.SelectedItem;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(DateTime.TryParse(textBox4.Text, out _) && int.TryParse(textBox3.Text, out _) &&
                comboBox3.SelectedIndex >= 0 && comboBox3.SelectedIndex < comboBox3.Items.Count &&
                comboBox2.SelectedIndex >= 0 && comboBox2.SelectedIndex < comboBox2.Items.Count)
            {
                connection.AddNewVerification(DateTime.Parse(textBox4.Text), int.Parse(textBox3.Text),
                    Workers[comboBox3.SelectedIndex].WorkerID, Factories[comboBox2.SelectedIndex].FactoryID);

                ConversionVerifications();
            }
            else
            {
                MessageBox.Show("Данные введены неверно!");
            }

            textBox3.Text = textBox4.Text = "";
        }
    }
}
